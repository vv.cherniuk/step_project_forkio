const burgerButton = document.getElementById("burger__button");
const navMenu = document.querySelector(".header__navbar__list");

window.addEventListener("click", (event) => {
  if (
    event.target === burgerButton ||
    event.target === burgerButton.children[0]
  ) {
    navMenu.classList.toggle("header__navbar__list--display");
    burgerButton.children[0].classList.toggle("fa-bars");
    burgerButton.children[0].classList.toggle("fa-xmark");
  } else if (!navMenu.classList.contains("header__navbar__list--display")) {
    navMenu.classList.add("header__navbar__list--display");
    burgerButton.children[0].classList.toggle("fa-xmark");
    burgerButton.children[0].classList.toggle("fa-bars");
  }
});

window.addEventListener("resize", (e) => {
  if (!navMenu.classList.contains("header__navbar__list--display")) {
    navMenu.classList.add("header__navbar__list--display");
    burgerButton.children[0].classList.toggle("fa-xmark");
    burgerButton.children[0].classList.toggle("fa-bars");
  }
});
